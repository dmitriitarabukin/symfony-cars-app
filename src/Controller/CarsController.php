<?php

namespace App\Controller;

use App\Document\Car;
use App\Form\CarType;
use Doctrine\ODM\MongoDB\DocumentManager as DocumentManager;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Serializer\Serializer;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\Serializer\Encoder\JsonEncode;
use Symfony\Component\Serializer\Encoder\JsonDecode;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Encoder\CsvEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;
use Symfony\Component\HttpFoundation\File\MimeType\FileinfoMimeTypeGuesser;

class CarsController extends AbstractController
{
	const AVAILABLE_IMPORT_FORMATS = ['json', 'csv'];

	/**
	* @Route("/cars")
	*/
	public function indexAction(Request $request, DocumentManager $documentManager)
	{
		$car = new Car();		
    $form = $this->createForm(CarType::class, $car);
    $cars = $this->getAllCars($documentManager);

	  return $this->render('cars/index.html.twig', [
	  	'cars' => $cars,
	  	'form' => $form->createView(),
	  ]);
	}

	/**
	* @Route("/cars/add", methods={"POST"})
	*/
	public function addAction(Request $request, DocumentManager $documentManager)
	{
		$car = new Car();
		$form = $this->createForm(CarType::class, $car);

		$form->handleRequest($request);

    if ($form->isSubmitted() && $form->isValid()) {
        $car = $form->getData();
				$documentManager->persist($car);
				$documentManager->flush();

        return $this->render('cars/car_row.html.twig', ['car' => $car]);
    }

    // There was a validation error
    if ($request->isXmlHttpRequest()) {
        $html = $this->renderView('cars/form.html.twig', [
            'form' => $form->createView()
        ]);

        return new Response($html, 400);
    }
	}

	/**
	* @Route("/cars/remove", methods={"DELETE"})
	*/
	public function removeAction(Request $request, DocumentManager $documentManager)
	{
		if ($request->isXmlHttpRequest()) {
			$repository = $documentManager->getRepository(Car::class);
			$id = $request->request->get('id');
			$car = $repository->findOneById($id);

			if (!$car) {
				throw $this->createNotFoundException('Машина с id: '.$id.' не найдена.');
			}
	
			$documentManager->remove($car);
			$documentManager->flush();
	
			return new Response('Машина удалена.', 200);
		}
	}

	/**
	* @Route("/cars/update", methods={"PUT"})
	*/
	public function updateAction(Request $request, DocumentManager $documentManager)
	{
		if ($request->isXmlHttpRequest()) {
			$repository = $documentManager->getRepository(Car::class);
			$id = $request->request->get('id');
			$car = $repository->findOneById($id);

			if (!$car) {
				throw $this->createNotFoundException('Машина с id: '.$id.' не найдена.');
			}

			$newBrand = $request->request->get('brand');
			$newModel = $request->request->get('model');
			$newWheel = $request->request->get('wheel');

			$car->setBrand($newBrand);
			$car->setModel($newModel);
			$car->setWheel($newWheel);
			$documentManager->flush();

			return $this->render('cars/car_row.html.twig', ['car' => $car]);
		}
	}

	/**
	* @Route("/cars/import/{format}", methods={"GET"})
	*/
	public function importAction(Request $request, DocumentManager $documentManager, $format)
	{
		if (!in_array($format, CarsController::AVAILABLE_IMPORT_FORMATS)) {
			$response = 'Данный формат импорта не поддерживается. Поддерживаемые форматы: '.
				implode(", ", CarsController::AVAILABLE_IMPORT_FORMATS).'.';
			return new Response($response, 400);
		}

		$cars = $this->getAllCars($documentManager);

		$encoders = [
			new JsonEncoder(new JsonEncode(JSON_UNESCAPED_UNICODE), new JsonDecode(false)),
			new CsvEncoder()
		];
		$normalizer = new ObjectNormalizer();
		$serializer = new Serializer(array($normalizer), $encoders);

		$filename = "cars.$format";
		$serializedData = $serializer->serialize($cars, $format);
		$filesystem = new Filesystem();
		$filesystem->dumpFile($filename, $serializedData);		

		$publicResourcesFolderPath = $this->getParameter('kernel.project_dir') . '/public/';
    $response = new BinaryFileResponse($publicResourcesFolderPath.$filename);
    $mimeTypeGuesser = new FileinfoMimeTypeGuesser();
    $response->headers->set('Content-Type', $mimeTypeGuesser->guess($publicResourcesFolderPath.$filename));
    $response->setContentDisposition(
    	ResponseHeaderBag::DISPOSITION_ATTACHMENT,
    	$filename
    );

    return $response;
	}

	private function getAllCars($documentManager)
	{
		$repository = $documentManager->getRepository(Car::class);
	  $cars = $repository->findAll();
	  return $cars;
	}
}