<?php

namespace App\Form;

use App\Document\Car;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class CarType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
        	->add('brand', TextType::class)
        	->add('model', TextType::class)
        	->add('wheel', ChoiceType::class, [
        		'choices' => [
        			'Выберите положение руля' => '',
        			'Левый руль' => 'Левый руль',
        			'Правый руль' => 'Правый руль',
        		],
        		'choice_attr' => function($choice, $key, $value) {
        			if ('Выберите положение руля' === $key) {
        			    return ['disabled' => true];
        			}        				
        			return [];
        		}
        	])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
		{
    	$resolver->setDefaults([
    	    'data_class' => Car::class,
    	]);
		}
}