<?php

namespace App\Document;

use Doctrine\ODM\MongoDB\Mapping\Annotations as MongoDB;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @MongoDB\Document
 */
class Car
{
    const WHEEL_TYPES = ['Левый руль', 'Правый руль'];
    /**
    * @MongoDB\Id
    */
    protected $id;

    /**
    * @MongoDB\Field(type="string")
    * @Assert\NotBlank(message="car.brand.not_blank")
    */
    protected $brand;

    /**
    * @MongoDB\Field(type="string")
    * @Assert\NotBlank(message="car.model.not_blank")
    */
    protected $model;

    /**
    * @MongoDB\Field(type="string")
    * @Assert\Choice(choices=Car::WHEEL_TYPES, message="car.wheel.not_blank")
    */
    protected $wheel;

    public function getId()
    {
        return $this->id;
    }

    public function getBrand()
    {
        return $this->brand;
    }

    public function setBrand($brand)
    {
        $this->brand = $brand;
    }

    public function getModel()
    {
        return $this->model;
    }

    public function setModel($model)
    {
        $this->model = $model;
    }

    public function getWheel()
    {
        return $this->wheel;
    }

    public function setWheel($wheel)
    {
        $this->wheel = $wheel;
    }
}