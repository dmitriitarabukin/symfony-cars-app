import $ from 'jquery';
import M from 'materialize-css';

var Elements = {
	$wrapper: null,
	$form: null,
	$form_title: null,
	$add_car: null,
	$add_car_preloader: null,
	$remove_car_preloader: null,
	$input_wheel: null,
	$table: null,
	$table_body: null,
	$input_errors_wrapper: null,
	$modal: null,
	$submit_remove_car: null,
	$update_car_buttons: null,
	$cancel_update_car: null,
	$submit_update_car: null
};

var Selectors = {
	wrapper: "[rel='js-cars-wrapper']",
	form: "[rel='js-form_add-car']",
	form_title: "[rel='js-form_add-car-title']",
	add_car: "[rel='js-add-car']",
	add_car_preloader: "[rel='js-add-car-preloader']",
	remove_car_preloader: "[rel='js-remove-car-preloader']",
	input_brand: "[rel='js-input-brand']",
	input_model: "[rel='js-input-model']",
	input_wheel: "[rel='js-input-wheel']",
	table: "[rel='js-table_cars']",
	table_body: "[rel='js-table_cars-body']",
	car_row: "[rel='js-car-row']",
	input_errors_wrapper: "[rel='js-input-errors']",
	input_errors: "[rel$='-error']",
	remove_car: "[rel='js-remove-car']",
	update_car: "[rel='js-update-car']",
	cancel_update_car: "[rel='js-cancel-update-car']",
	modal: "[rel='js-cars-form-modal']",
	submit_remove_car: "[rel='js-cars-form-modal-yes']",
	update_car_buttons: "[rel='js-update-car-buttons']",
	submit_update_car: "[rel='js-submit-update-car']"
};

const PRELOADER_TYPES = {
	add_car: 'add-car',
	update_car: 'add-car',
	remove_car: 'remove-car'
};

const FORM_TITLES = {
	add_car: 'Добавление машины',
	update_car: 'Обновление информации о машине'
};

const BUTTON_TYPES = {
	add_car: 'add',
	update_car: 'update'
};

function init() {
	Elements.$wrapper = $(Selectors.wrapper);
	Elements.$form = $(Selectors.form);
	Elements.$form_title = $(Selectors.form_title);
	Elements.$add_car = $(Selectors.add_car);
	Elements.$cancel_update_car = $(Selectors.cancel_update_car);
	Elements.$add_car_preloader = $(Selectors.add_car_preloader);
	Elements.$remove_car_preloader = $(Selectors.remove_car_preloader);
	Elements.$input_brand = $(Selectors.input_brand);
	Elements.$input_model = $(Selectors.input_model);
	Elements.$input_wheel = $(Selectors.input_wheel);
	Elements.$table = $(Selectors.table);
	Elements.$table_body = $(Selectors.table_body);
	Elements.$input_errors_wrapper = $(Selectors.input_errors_wrapper);
	Elements.$modal = $(Selectors.modal);
	Elements.$submit_remove_car = $(Selectors.submit_remove_car);
	Elements.$update_car_buttons = $(Selectors.update_car_buttons);
	Elements.$submit_update_car = $(Selectors.submit_update_car);
}

function clearErrors() {
	Elements.$input_errors_wrapper.html('');
}

function showErrors($errors) {
	Elements.$input_errors_wrapper.html($errors);
}

function addCarRow(row) {
	Elements.$table_body.append(row);
}

function removeCarRow($row) {
	$row.remove();
}

function updateCarRow(id, row) {
	Elements.$table_body.find(Selectors.car_row + `[data-id='${id}']`).replaceWith(row);
}

function togglePreloader(type) {
	switch(type) {
		case PRELOADER_TYPES.add_car:
			Elements.$add_car_preloader.toggleClass('hidden');
			break;
		case PRELOADER_TYPES.remove_car:
			Elements.$remove_car_preloader.toggleClass('hidden');
			break;
	}
}

function startUpdateCar(car_properties) {
	_showUpdateCarButtons();
	_setFormTitle(FORM_TITLES.update_car);
	Elements.$form_title.text(FORM_TITLES.update_car);
	Elements.$input_brand.val(car_properties.brand);
	Elements.$input_model.val(car_properties.model);
	Elements.$input_wheel.val(car_properties.wheel);
	_reinitializeInputs();
}

function cancelUpdateCar() {
	_hideUpdateCarButtons();
	_setFormTitle(FORM_TITLES.add_car);
	Elements.$input_brand.val('');
	Elements.$input_model.val('');
	Elements.$input_wheel.val('');
	_reinitializeInputs();
}

function disableButtons(type) {
	switch(type) {
		case BUTTON_TYPES.add_car:
			Elements.$add_car.prop('disabled', true);
			Elements.$add_car.prop('disabled', true);
			break;
		case BUTTON_TYPES.update_car:
			Elements.$submit_update_car.prop('disabled', true);
			Elements.$cancel_update_car.prop('disabled', true);
			break;
	}
}

function enableButtons(type) {
	switch(type) {
		case BUTTON_TYPES.add_car:
			Elements.$add_car.prop('disabled', false);
			Elements.$add_car.prop('disabled', false);
			break;
		case BUTTON_TYPES.update_car:
			Elements.$submit_update_car.prop('disabled', false);
			Elements.$cancel_update_car.prop('disabled', false);
			break;
	}
}

function _setFormTitle(title) {
	Elements.$form_title.text(title);
}

function _showUpdateCarButtons() {
	Elements.$add_car.addClass('hidden');
	Elements.$update_car_buttons.removeClass('hidden');		
}

function _hideUpdateCarButtons() {
	Elements.$add_car.removeClass('hidden');
	Elements.$update_car_buttons.addClass('hidden');				
}

function _reinitializeInputs() {
	M.updateTextFields();
	M.FormSelect.init(Elements.$input_wheel);
}

export {
	init,
	Elements,
	Selectors,
	clearErrors,
	addCarRow,
	removeCarRow,
	togglePreloader,
	startUpdateCar,
	cancelUpdateCar,
	updateCarRow,
	disableButtons,
	enableButtons,
	PRELOADER_TYPES,
	BUTTON_TYPES	
};