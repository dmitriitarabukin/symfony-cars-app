import $ from 'jquery';
import { Elements } from './UIController.js';

const ACTIONS = {
	add: '/cars/add',
	remove: '/cars/remove',
	update: '/cars/update'
};

function addCar(resolve) {
	$.ajax({
		method: "POST",
		url: ACTIONS.add,
		data: Elements.$form.serialize(),
		success: function(response) {
			resolve({data: response, is_success: true});
		},
		error: function(jqXHR) {
			resolve({data: jqXHR.responseText, is_success: false});
		}
	});
}

function removeCar(id, resolve) {
	$.ajax({
		method: "DELETE",
		url: ACTIONS.remove,
		data: {id: id},
		success: function(response) {
			resolve(response);
		},
		error: function(error) {
			console.log(error);
		}
	});
}

function updateCar(new_car_properties, resolve) {
	$.ajax({
		method: "PUT",
		url: ACTIONS.update,
		data: {
			id: new_car_properties.id,
			brand: new_car_properties.brand,
			model: new_car_properties.model,
			wheel: new_car_properties.wheel
		},
		success: function(response) {
			resolve(response);
		},
		error: function(error) {
			console.log(error);
		}
	});
}


export {
	addCar,
	removeCar,
	updateCar
};