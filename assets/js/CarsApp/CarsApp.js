import $ from 'jquery';
import M from 'materialize-css';
import {
	init as initUIController,
	Elements,
	Selectors,
	clearErrors,
	addCarRow,
	removeCarRow,
	togglePreloader,
	startUpdateCar,
	cancelUpdateCar,
	updateCarRow,
	disableButtons,
	enableButtons,
	PRELOADER_TYPES,
	BUTTON_TYPES
} from './UIController.js';
import {
	addCar,
	removeCar,
	updateCar
} from './RequestController.js';

var modal, selected_car_id, $selected_car_row;

function init() {
	initUIController();
	M.FormSelect.init(Elements.$input_wheel);
	modal = M.Modal.init(document.querySelector(Selectors.modal));

	Elements.$form.on('submit', _addCarHandler);
	Elements.$table_body.on('click', Selectors.remove_car, _openRemoveCarModalHandler);
	Elements.$table_body.on('click', Selectors.update_car, _startUpdateCarHandler);
	Elements.$cancel_update_car.on('click', _cancelUpdateCarHandler);
	Elements.$submit_remove_car.on('click', _removeCarHandler);
	Elements.$submit_update_car.on('click', _updateCarHandler);
}

function _addCarHandler(event) {
	event.preventDefault();
	disableButtons(BUTTON_TYPES.add_car);
	togglePreloader(PRELOADER_TYPES.add_car);
	var add_car_promise = new Promise(function(resolve) {
		addCar(resolve);
	})
	.then(function(response) {
		if (response.is_success) {
			let car_row = response.data;
			addCarRow(car_row);
			clearErrors();
			enableButtons(BUTTON_TYPES.add_car);
			togglePreloader(PRELOADER_TYPES.add_car);
		} else {
			let $response_data = $(response.data),
					$errors = $response_data.find(Selectors.input_errors);
			showErrors($errors);
			enableButtons(BUTTON_TYPES.add_car);
			togglePreloader(PRELOADER_TYPES.add_car);
		}
	}).catch(function(error) {
		togglePreloader(PRELOADER_TYPES.remove_car);
		console.log(error);
	});
}

function _openRemoveCarModalHandler(event) {
	event.preventDefault();
	modal.open();
	_selectCar(event.target);
}

function _removeCarHandler(event) {
	event.preventDefault();

	togglePreloader(PRELOADER_TYPES.remove_car);

	var remove_car_promise = new Promise(function(resolve) {
		removeCar(selected_car_id, resolve);
	})
	.then(function(response) {
		removeCarRow($selected_car_row);
		togglePreloader(PRELOADER_TYPES.remove_car);
		modal.close();
	}).catch(function(error) {
		togglePreloader(PRELOADER_TYPES.remove_car);
		console.log(error);
	});		
}

function _startUpdateCarHandler(event) {
	_selectCar(event.target);
	{ let car_properties = {
			brand: $selected_car_row.data('brand'),
			model: $selected_car_row.data('model'),
			wheel: $selected_car_row.data('wheel')
		};
		startUpdateCar(car_properties);
	}
}

function _cancelUpdateCarHandler(event) {
	event.preventDefault();
	cancelUpdateCar();
}

function _updateCarHandler(event) {
	event.preventDefault();
	var new_car_brand = Elements.$input_brand.val(),
			new_car_model = Elements.$input_model.val(),
			new_car_wheel = Elements.$input_wheel.val();
			
	if (!new_car_brand || !new_car_model || !new_car_wheel) return;

	disableButtons(BUTTON_TYPES.update_car);
	togglePreloader(PRELOADER_TYPES.update_car);

	var update_car_promise = new Promise(function(resolve) {
		updateCar({
			id: selected_car_id,
			brand: new_car_brand, 
			model: new_car_model,
			wheel: new_car_wheel
		}, resolve);
	})
	.then(function(response) {
		updateCarRow(selected_car_id, response);
		enableButtons(BUTTON_TYPES.update_car);
		togglePreloader(PRELOADER_TYPES.update_car);
	}).catch(function(error) {
		enableButtons(BUTTON_TYPES.update_car);
		togglePreloader(PRELOADER_TYPES.update_car);
		console.log(error);
	})
}

function _selectCar(event_target) {
	$selected_car_row = $(event_target).parents(Selectors.car_row);
	selected_car_id = $selected_car_row.data('id');
}

export { init };